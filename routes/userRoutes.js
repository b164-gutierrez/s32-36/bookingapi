const express = require("express");
const router = express.Router();
const auth = require("../auth");


const UserController = require("../controllers/userControllers");

//Route for checking if the user's email already exists in the database

router.post("/checkEmail", (req, res) => {
	UserController.checkEmailExists(req.body).then(result => res.send(result));
});


// Registration for user.
// http://localhost:4000/api/users/register
router.post('/register', (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result));
})


// User Authentication (Login)
router.post('/login', (req, res) =>{
	UserController.loginUser(req.body).then(result => res.send(result));
})


// User Details
// router.get('/details', (req, res) => {
// 	UserController.getDetails(req.body).then(result => res.send(result));
// })


// Activity===========================

// router.get('/details/:id', (req, res) => {
// 	UserController.getProfile(req.params.id).then(result => res.send(result));
// })

// ============================================


// 'auth.verify' acts as a middleware to ensure that the user is logged in before they can get the details of a user.
router.get('/details', auth.verify, (req, res) => {

	// decode() to retrieve the user information from the token passing the 'token' from the request headers as an argument.

	const userData = auth.decode(req.headers.authorization);

	UserController.getProfile(userData.id).then(result => res.send(result));
})




// Enroll User to a course
router.post('/enroll', auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId : req.body.courseId
	}

	UserController.enroll(data).then(result => res.send(result));

})



module.exports = router;
