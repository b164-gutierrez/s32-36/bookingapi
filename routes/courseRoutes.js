const express = require('express');
const router = express.Router();
const CourseController = require('../controllers/courseControllers');

const auth = require("../auth");

// const User = require("../models/User");


// Creating a course.
// router.post('/', auth.verify, (req, res) => {

// 	const userData = auth.decode(req.headers.authorization);

// 	CourseController.addCourse(req.body).then(result => res.send(result));
// })


// activity=======================
/*
router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin) {
		CourseController.addCourse(data).then(result => res.send(result));
	} else {
		res.send({ auth: "You're not an admin"})
	}
})
*/




// ============================
// router.get('/', auth.verify, (req, res) => {

// 	// decode() to retrieve the user information from the token passing the 'token' from the request headers as an argument.

// 	const userData = auth.decode(req.headers.authorization);

// 	UserController.getProfile(userData.id).then(result => res.send(result));
// })


router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin) {
		CourseController.addCourse(data).then(result => res.send(result));
	} else {
		res.send({ auth: "You're not an admin"})
	}
})




// Retrieving All courses.
router.get("/all", (req, res) => {
	CourseController.getAllCourses().then(result => res.send(result));
})



//Retrieving all ACTIVE courses
router.get("/", (req, res) => {
	CourseController.getAllActive().then(result => res.send(result))
});



// Retrieving a Specific course.
router.get('/:courseId', (req, res) =>{
	console.log(req.params.courseId);

	CourseController.getCourse(req.params.courseId).then(result => res.send(result));
})




// Update a course
router.put('/:courseId', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		CourseController.updateCourse(req.params.courseId, req.body).then(result => res.send(result));
	} else {
		res.send(false);
	}
})



// Update isActive true to false
router.put('/:courseId/archive', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		CourseController.updateIsActive(req.params.courseId, req.body).then(result => res.send(result));
	} else {
		res.send(false);
	}
})




// Enroll User to a course
router.post('/enroll', auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId : req.body.courseId
	}
})









module.exports = router;











