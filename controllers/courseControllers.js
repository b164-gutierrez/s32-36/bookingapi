const Course = require('../models/Course');

const auth = require('../auth');
// const User = require("../models/User");

// Create a new course.

/*
	Steps:
		1. Create a new Course object.
		2. Save to the database.
		3. error handling.
*/




// module.exports.addCourse = (reqBody) => {
	

// 	return Course.find({ isAdmin: reqBody.isAdmin }).then(result => {

// 		//if match is found
// 		if(result.length == true){

// 			let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	});
// 			return newCourse.save().then((course, error) => {
// 		// Course creation failed.
// 		if(error) {
// 			return false;
// 		} else {
// 			// Course creation successful.
// 			return true;
// 		}
// 	})

// 		} else {
			
// 			return false;
// 		}
// 	})
// }

	// Create a new object
	

	// Saves the created object to our database.
	



// Activity
// If admin = true, add course
// ==========================================
// module.exports.addCourse = (reqBody) => {
// 	console.log(reqBody);

// 	// Create a new object
// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	});

// 	// Saves the created object to our database.
// 	return newCourse.save().then((course, error) => {
// 		// Course creation failed.
// 		if(error) {
// 			return false;
// 		} else {
// 			// Course creation successful.
// 			return true;
// 		}
// 	})
// }
// ==================================





module.exports.addCourse = (reqBody) => {
	console.log(reqBody);


	// For checking duplicate course
	// return Course.find({reqBody}).then(result => {
	// 	if(reqBody.length > 0) {
	// 		return
	// 	}
	// })

	//Create a new object
	let newCourse = new Course({
		name: reqBody.course.name,
		description: reqBody.course.description,
		price: reqBody.course.price
	});

	//Saves the created object to our database
	return newCourse.save().then((course, error) => {
		//Course creation failed
		if(error) {
			return false;
		} else {
			//Course Creation successful
			return true;
		}
	})

}




// Retrieve All courses.
module.exports.getAllCourses = () => {
	return Course.find({}).then( result => {
		return result;
	})
}



//Retrieve all ACTIVE courses.
module.exports.getAllActive = () => {
	return Course.find({ isActive: true }).then(result => {
		return result;
	})
}




// Retrieve a Specific course.
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result;
	})
}




// Update a course
/*
	Steps:
		1. Create variable which will contain the information retrieved from the request body.
		2. Find and Update course using the course ID.
*/
module.exports.updateCourse = (courseId, reqBody) => {
	// specify the properties of the document to be updated.
	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		isActive: reqBody.isActive
	};

	// .findByIdAndUpdate (id, updatesToBeApplied)
	return Course.findByIdAndUpdate(courseId, updateCourse).then((course, error) => {

		// course not updated.
		if(error) {
			return false;
		} else {
			// course updated successfully
			return true;
		}

	})
}



// Update isActive
/*
	Steps:
		1. Create variable which will contain the information retrieved from the request body.
		2. Find and Update course using the course ID.
*/
module.exports.updateIsActive = (courseId, reqBody) => {
	// specify the properties of the document to be updated.
	let updateIsActive = {
		
		isActive: reqBody.isActive
	};

	// .findByIdAndUpdate (id, updatesToBeApplied)
	return Course.findByIdAndUpdate(courseId, updateIsActive).then((course, error) => {

		// course not updated.
		if(error) {
			return false;
		} else {
			// course updated successfully
			return true;
		}

	})
}









