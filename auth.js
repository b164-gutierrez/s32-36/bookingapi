const jwt = require('jsonwebtoken');
const secret = 'CrushAkoNgCrushKo';


// JSON Web Token (JWT) is a way securely passing information from the server to the front end or to other parts of server.
// Information is kept secure through the use of secret code.
// Only the system that knows the secret code that can decode the encrypted information.


// Token Creation
/*
	Analogy:
		-Pack the gift and provide lock with the secret code as the key.

*/


module.exports.createAccessToken = (user) => {
	// The data will be recieved from the registration form.
	// when the user logs in, a token will be created with user's information.

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	// Generate a JSON web token using the jwt's method (sign()).

	return jwt.sign(data, secret, {}); // {} = option ex: 'session timeout'
}


// Token Verification

/*
	Analogy:
		- Recieve the gift and open the lock to verify if the sender is legitimate and the gift was not tampered.
*/

module.exports.verify = (req, res, next) => {

		// token is retrieved from the request header.
		let token = req.headers.authorization;

		// if token is recieved and is not undefined.
		if(typeof token !== 'undefined'){

			console.log(token);
			// Bearer 2b$10$Hv0ob74EX6o. (to remove 'Bearer' string use slice method)
			token = token.slice(7, token.length);

			// Validate the token using the 'verify' method, decrypting the token using secret method.

			return jwt.verify(token, secret, (err, data)=> {
				// if JWT is not valid
				if(err) {
					return res.send({auth: 'failed'});
				} else {
					// if JWT is valid
					next()
				}
			})


		} else {

			// if token does not exist
			return res.send({ auth: 'token undefined' })
		}

}



// Token decryption
/*
	Analogy:
		Open the gift and get the content.
*/

module.exports.decode = (token) => {

	// Token recieved and is not undefined.
	if(typeof token !== 'undefined'){

		// Retrieves only the token and removes the 'Bearer' prefix.
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return null;
			}	else {
				return jwt.decode(token, {complete: true}).payload; //payload = data
			}
		})

	} else {
		// token does not exist
		return null
	}

}